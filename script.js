/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */
/*
1. Події в JavaScript - це взаємодія користувача з веб-сторінкою чи дії, що відбуваються на сторінці. 
Вони дозволяють викликати функції чи обробники подій відповідно до дій користувача або інших умов. 
Наприклад, клік миші, натискання клавіші, завантаження сторінки - це події.
    
2. Події миші в JavaScript:
Декілька прикладів подій миші (click, mousedown, mouseup, mousemove )

3. Подія "contextmenu":
Подія "contextmenu" спрацьовується при відкритті контекстного меню (правий клік миші) на елементі сторінки.
Вона дозволяє виконувати певні дії або відобразити власне контекстне меню.
*/
 


// Практичні завдання

// 1
const button = document.querySelector("#btn-click");
const section = document.querySelector("#content");
button.addEventListener("click", () => {
    const p = document.createElement('p');
    p.innerText = "New Paragraph";
    section.append(p);


// 2
const buttonCreateInput = document.createElement('button');
buttonCreateInput.id = "btn-input-create";
buttonCreateInput.innerText = "Сreate input"

const sectionContent = document.querySelector("#content");
sectionContent.append(buttonCreateInput);

buttonCreateInput.addEventListener("click", () => {
    const input = document.createElement("input");
    input.type = "text";
    input.name = "name";
    input.placeholder = "Enter your name";
    buttonCreateInput.after(input);
})
